﻿using System.Collections.Generic;

namespace NaturalEngine.Core.Components.Context
{
    /// <summary>
    /// Mutable context of subsequent initialization calls
    /// </summary>
    public class InitializationContext
    {
        /// <summary>
        /// Dictionary of properties that can be set during initialization calls and passed on
        /// </summary>
        public IDictionary<string, object> Properties = new Dictionary<string, object>();
    }
}