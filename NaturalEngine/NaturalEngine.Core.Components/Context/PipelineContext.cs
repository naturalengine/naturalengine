﻿using System.Collections.Generic;

namespace NaturalEngine.Core.Components.Context
{
    /// <summary>
    /// Mutable context for a single pass through the pipeline
    /// </summary>
    public class PipelineContext
    {
        /// <summary>
        /// Dictionary of properties that can be set during pipeline calls and passed on
        /// </summary>
        public IDictionary<string, object> Properties = new Dictionary<string, object>(); 
    }
}