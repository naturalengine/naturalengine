﻿using System.Collections.Generic;

namespace NaturalEngine.Core.Components.Context
{
    /// <summary>
    /// Mutable context of change handler calls
    /// </summary>
    public class ChangeHandlerContext
    {
        /// <summary>
        /// Dictionary of properties that can be set during change handler calls and passed on
        /// </summary>
        public IDictionary<string, object> Properties = new Dictionary<string, object>();
    }
}