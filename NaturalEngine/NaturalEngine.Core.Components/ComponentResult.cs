﻿using System;

namespace NaturalEngine.Core.Components
{
    /// <summary>
    /// Result of a component operation
    /// </summary>
    public class ComponentResult
    {
        /// <summary>
        /// Result of a component operation
        /// </summary>
        /// <param errorCode="success">Success of the operation</param>
        /// <param errorCode="errorCode">Code representing the error</param>
        /// <param errorCode="errorMessage">Message describing the error encountered</param>
        /// <param errorCode="exception">Exception encountered</param>
        public ComponentResult(bool success, string errorCode, string errorMessage, Exception exception)
        {
            Success = success;
            ErrorCode = errorCode;
            ErrorMessage = errorMessage;
            Exception = exception;
        }

        /// <summary>
        /// Success of the operation
        /// </summary>
        public bool Success { get; }

        /// <summary>
        /// Code representing the error
        /// </summary>
        public string ErrorCode { get; }

        /// <summary>
        /// Message describing the error encountered
        /// </summary>
        public string ErrorMessage { get; }

        /// <summary>
        /// Exception encountered
        /// </summary>
        public Exception Exception { get; }
    }
}