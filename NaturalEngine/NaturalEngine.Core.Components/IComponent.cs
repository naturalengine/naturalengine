﻿using System.Threading.Tasks;
using NaturalEngine.Core.Components.Context;

namespace NaturalEngine.Core.Components
{
    /// <summary>
    /// A core component
    /// </summary>
    public interface IComponent
    {
        /// <summary>
        /// Run the component
        /// </summary>
        /// <param name="pipelineContext">Current PipelineContext defined by the pipeline</param>
        /// <returns>Result of the operation</returns>
        Task<ComponentResult> Run(PipelineContext pipelineContext);
    }
}