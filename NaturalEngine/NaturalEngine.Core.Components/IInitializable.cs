﻿using NaturalEngine.Core.Components.Context;

namespace NaturalEngine.Core.Components
{
    /// <summary>
    /// Used when a component required initialization
    /// </summary>
    public interface IInitializable
    {
        /// <summary>
        /// Initialize a component
        /// </summary>
        /// <param name="initializationContext">Current initialization context</param>
        /// <returns>Result of the initialization</returns>
        ComponentResult Initialize(InitializationContext initializationContext);
    }
}