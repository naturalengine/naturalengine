﻿using NaturalEngine.Core.Components.Context;

namespace NaturalEngine.Core.Components
{
    /// <summary>
    /// Component which is requires action on change
    /// </summary>
    public interface IChangeAware
    {
        /// <summary>
        /// </summary>
        /// <param name="context">Context of the change calls</param>
        /// <returns>Result of the change operation</returns>
        ComponentResult HandleChange(ChangeHandlerContext context);
    }
}