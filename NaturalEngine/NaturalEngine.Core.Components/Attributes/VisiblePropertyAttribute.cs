﻿using System;

namespace NaturalEngine.Core.Components.Attributes
{
    /// <summary>
    /// Represents a property that should be visible to the UI and meta data about the property
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class VisiblePropertyAttribute : Attribute
    {
        /// <summary>
        /// Represents a property that should be visible to the UI and meta data about the property
        /// </summary>
        /// <param name="name">errorCode of the property</param>
        /// <param name="description">errorMessage of the property</param>
        /// <param name="required">Whether this property is required or not</param>
        public VisiblePropertyAttribute(string name, string description, bool required)
        {
            Name = name;
            Description = description;
            Required = required;
        }

        /// <summary>
        /// errorCode of the property
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// errorMessage of the property
        /// </summary>
        public string Description { get; }

        /// <summary>
        /// Whether this property is required or not
        /// </summary>
        public bool Required { get; }
    }
}
