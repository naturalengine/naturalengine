﻿using System;

namespace NaturalEngine.Core.Components.Attributes
{
    /// <summary>
    /// Annotates a visible component to provide meta data about the component
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class VisibleComponentAttribute : Attribute
    {
        /// <summary>
        /// Annotate a visible component to provide meta data about the component
        /// </summary>
        /// <param name="name">errorCode of the component</param>
        /// <param name="description">errorMessage of the component</param>
        public VisibleComponentAttribute(string name, string description)
        {
            Name = name;
            Description = description;
        }

        /// <summary>
        /// errorCode of the component
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// errorMessage of the component
        /// </summary>
        public string Description { get; }
    }
}
